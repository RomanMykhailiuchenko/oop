public class Main {

    public static void main(String[] args)  {
        Shkola myShkola = new Shkola();
        myShkola.calculateSrokObuchenia(myShkola.getSrednee());
        myShkola.setObrazovanie(myShkola, Obrazovanie.SREDNEE);
        myShkola.getObrazovanie();

        University myUniversity = new University();
        myUniversity.calculateSrokObuchenia(myUniversity.getObrazovanie());
        myUniversity.setObrazovanie(myUniversity,Obrazovanie.VYSSHEE);
        myUniversity.getObrazovanie();

    }
}
